package bobkov.com.githubresume.common

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import bobkov.com.githubresume.data.Repository

open class BaseViewModel(application: Application, protected val repository: Repository) :
    AndroidViewModel(application)