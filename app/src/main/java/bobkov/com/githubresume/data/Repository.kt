package bobkov.com.githubresume.data

import androidx.lifecycle.LiveData
import bobkov.com.githubresume.data.entity.User

interface Repository {

    fun getUser(nickname: String): LiveData<ResultObject<User>>
}