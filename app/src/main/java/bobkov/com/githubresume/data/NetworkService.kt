package bobkov.com.githubresume.data

import bobkov.com.githubresume.data.entity.User

interface NetworkService {

    suspend fun getUser(nickName: String): ResultObject<User>
}