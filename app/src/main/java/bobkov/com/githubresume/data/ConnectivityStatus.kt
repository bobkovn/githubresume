package bobkov.com.githubresume.data

interface ConnectivityStatus {

    fun hasConnection() : Boolean
}